import React from "react";
import "./Cart.scss";
import Card from "../../components/Card/Card";
import { getCart } from "../../store/operations";
import { useSelector } from "react-redux";

const Cart = () => {
  const products = useSelector((state) => state.products.data);
  const cart = products.filter((product) => product.inCartAmount);

  if (!getCart()) {
    return <div className="container">Cart is empty</div>;
  }

  return (
    <div className="container">
      {cart.map((product) => {
        return <Card key={product.articul} product={product} fromCart />;
      })}
    </div>
  );
};

export default Cart;
