import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import CardList from "../components/CardList/CardList";
import Page404 from "./Page404/Page404";
import Favorites from "./Favorites/Favorites";
import Cart from "./Cart/Cart";

const Routes = () => {
  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/shop" />
        <Route exact path="/shop" component={CardList} />
        <Route exact path="/favorites" component={Favorites} />
        <Route exact path="/cart" component={Cart} />
        <Route path="*" component={Page404} />
      </Switch>
    </>
  );
};

export default Routes;
