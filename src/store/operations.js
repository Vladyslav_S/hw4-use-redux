import axios from "axios";
import { LOAD_CARDS_SUCCESS } from "./types";

export const getFavorites = () => {
  const LSFavorites = localStorage.getItem("favorite");
  return LSFavorites ? JSON.parse(LSFavorites) : [];
};

export const setFavorites = (data) => {
  localStorage.setItem("favorite", JSON.stringify(data));
};

export const getCart = () => {
  const LSCart = localStorage.getItem("Cart");
  return LSCart ? JSON.parse(LSCart) : {};
};

export const setCart = (data) => {
  localStorage.setItem("Cart", JSON.stringify(data));
};

export const loadProducts = () => (dispatch) => {
  const favorites = getFavorites();
  const cart = getCart();

  axios("./products.json").then((res) => {
    const newProducts = res.data.map((product) => {
      product.isFavorite = favorites.includes(product.articul);
      product.inCartAmount = cart[product.articul] || null;
      return product;
    });
    dispatch({ type: LOAD_CARDS_SUCCESS, payload: newProducts });
  });
};
