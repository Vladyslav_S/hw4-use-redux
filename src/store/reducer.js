import {
  ADD_TO_CART,
  CLOSE_MODAL,
  LOAD_CARDS_SUCCESS,
  REMOVE_FROM_CART,
  SHOW_MODAL,
  TOGGLE_FAVORITE,
} from "./types";
import { getCart, getFavorites, setCart, setFavorites } from "./operations";

const initialState = {
  products: {
    data: [],
    isLoading: true,
  },
  modal: {
    info: "",
    data: null,
    isOpen: false,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_CARDS_SUCCESS:
      return {
        ...state,
        products: { ...state.data, data: action.payload, isLoading: false },
      };
    case TOGGLE_FAVORITE:
      const newArray = state.products.data.map((product) => {
        if (product.articul === action.payload) {
          product.isFavorite = !product.isFavorite;

          const favorites = getFavorites();
          if (favorites.includes(product.articul)) {
            favorites.splice(favorites.indexOf(product.articul), 1);
          } else {
            favorites.push(product.articul);
          }
          setFavorites(favorites);
        }
        return product;
      });
      return { ...state, products: { ...state.products, data: newArray } };

    case SHOW_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          info: action.payload.info,
          data: action.payload.data,
          isOpen: true,
        },
      };
    case CLOSE_MODAL:
      return { ...state, modal: { ...state.modal, isOpen: false } };
    case ADD_TO_CART:
      const newProducts = state.products.data.map((product) => {
        if (product.articul === action.payload) {
          product.inCartAmount++;

          const cart = getCart();
          if (Object.keys(cart).includes(product.articul)) {
            cart[product.articul]++;
          } else {
            cart[product.articul] = 1;
          }
          setCart(cart);
        }
        return product;
      });
      return { ...state, products: { ...state.products, data: newProducts } };
    case REMOVE_FROM_CART:
      const newProductsArray = state.products.data.map((product) => {
        if (product.articul === action.payload) {
          product.inCartAmount--;

          const cart = getCart();
          cart[product.articul]--;
          if (cart[product.articul] === 0) {
            delete cart[product.articul];
          }
          setCart(cart);
        }
        return product;
      });
      return {
        ...state,
        products: { ...state.products, data: newProductsArray },
      };
    default:
      return state;
  }
};

export default reducer;
